using UnityEngine;
using System;

[Serializable]
public enum State
{
    IDLE
};


public class GameController : MonoBehaviour
{
    public static GameController instance;
    private State state;
    private RaycastHit hit;
    public const int size = 5;

    void Awake()
    {
        MakeInstance();
    }

    private void MakeInstance()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    void Start()
    {
        state = State.IDLE;
        //CubeController.instance.start();
    }

    void Update()
    {
        if (state == State.IDLE && Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 50f))
            {
                Debug.Log("Player clicked " + hit.collider.name + " at [" + (int)hit.collider.transform.localPosition.x + "," + (int)hit.collider.transform.localPosition.z + "]");
            }
        }
    }
}