using UnityEngine;
using TMPro;
using UnityEngine.Assertions;

public class Cube : MonoBehaviour
{
    private TMP_Text tmpPositionText;

    void Start()
    {
        tmpPositionText = this.gameObject.transform.GetChild(0).transform.GetComponent<TMP_Text>();
        Assert.IsNotNull(tmpPositionText.gameObject);
    }

    void FixedUpdate()
    {
        tmpPositionText.text = (int)this.gameObject.transform.localPosition.x + "," + (int)this.gameObject.transform.localPosition.z;
    }
}