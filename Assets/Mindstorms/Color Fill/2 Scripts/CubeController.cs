using System.Collections;
using UnityEngine;


public class CubeController : MonoBehaviour
{
    public static CubeController instance;
    [SerializeField]
    private GameObject prefab;
    public GameObject[,] cubes;

    void Awake()
    {
        if (instance == null)
            instance = this;
    }

    public void start()
    {
        StartCoroutine(Spawn());
        Grid.instance.AdjustPosition();
    }

    public IEnumerator Spawn()
    {
        cubes = new GameObject[GameController.size, GameController.size];

        for (int row = 0; row < GameController.size; row++)
        {
            for (int column = 0; column < GameController.size; column++)
            {
                GameObject tile = Instantiate(prefab, this.gameObject.transform);
                tile.transform.localPosition = new Vector3(column, 0, row);
                cubes[row, column] = tile;
            }
        }
        yield return null;
    }

    public void PrintListOfList()
    {
        for (int column = 0; column < GameController.size; column++)
        {
            int count = 0;
            for (int row = 0; row < GameController.size; row++)
            {
                if (cubes[column, row] != null)
                {
                    count++;
                }
            }
            Debug.Log(count);
        }
    }
}
